from django.apps import AppConfig


class QueuecheckerConfig(AppConfig):
    name = 'queuehealth'
    label = 'queuehealth'
    verbose_name = "Queue Health Check"
